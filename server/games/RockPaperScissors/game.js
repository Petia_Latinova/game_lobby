const Player = require("./player");
const Board = require("./board");

class Game {
  constructor(name, maxPlayers) {
    this.name = name;
    this.maxPlayers = maxPlayers;
    this.sockets = {};
    this.players = {};
    this.board = {};
    this.isStart = false;
    this.canPlay = false;
    this.canJoin = true;
    this.winner = null;
    this.win_check = true;

    this.board = new Board(this.players);
    console.log("start new update interval...");
    this.win_interval = setInterval(this.update.bind(this), 1000 / 15);
  }

  addBot(name) {
    console.log("add bot");
    if (!this.canJoin) return;
    var botID = name;
    this.players[botID] = new Player(
      botID,
      botID,
      "https://api.adorable.io/avatars/" + botID,
      true
    );
  }

  addPlayer(socket, username, img) {
    console.log("add player");
    if (!this.canJoin) return;
    console.log("add player can join");
    this.sockets[socket.id] = socket;
    this.players[socket.id] = new Player(socket.id, username, img, false);
    console.log("players " + Object.keys(this.players).length);
    if (Object.keys(this.players).length === this.maxPlayers)
    {
      if (this.maxPlayers === 1) {
        (username === "bot1") ? this.addBot("bot2") : this.addBot("bot1");
        //(username === "bot2") ? this.addBot("bot3") : this.addBot("bot2");
        //(username === "bot3") ? this.addBot("bot4") : this.addBot("bot3");
      }
       this.preStart(true);
    }
  }

  removePlayer(socket) {
    delete this.sockets[socket.id];
    delete this.players[socket.id];
  }

  start() {
    this.isStart = true;
    this.canPlay = true;
    console.log("start players " + Object.keys(this.players).length);
    console.log("start players " + JSON.stringify(this.players));
    this.board = new Board(this.players);
    //clearInterval(this.win_interval);
    //this.win_interval = setInterval(this.update.bind(this), 1000 / 15);
    setTimeout(this.endgame.bind(this), 5400); //5 seconds plus handle up to 400 ms network delay
    this.game_time = 5;
    this.game_interval = setInterval(() => {
      this.game_time--;
      if (this.game_time === 0)
        clearInterval(this.game_interval);
    }, 1000);
  }

  preStart(newGame) {
    if (this.interval) return;
    console.log("preStart players " + JSON.stringify(this.players));
    Object.keys(this.players).forEach(player => {
      this.players[player].isPlayed = false;
    });
    if(this.winner !== null) {
      Object.keys(this.players).forEach(player => {
        this.players[player].isLost = false;
      });
      this.winner = null;
    }
    this.canJoin = false;
    var time = (newGame) ? 5 : 0;
    this.interval = setInterval(() => {
      Object.keys(this.sockets).forEach(playerID => {
        const socket = this.sockets[playerID];
        socket.emit("waiting", time);
      });
      time--;
      if (time === -1) {
        clearInterval(this.interval);
        this.interval = null;
        this.start();
      }
    }, 1000);
  }

  reset() {
    this.board = new Board(this.players);
    this.isStart = true;
    this.canJoin = true;

    if (this.winner && this.winner.id !== undefined) {
      Object.keys(this.players).forEach(player => {
        this.players[player].isLost = false;
      });
    }
    let newGame = (this.winner && this.winner.id !== undefined) ? true : false;
    this.winner = null;
    if (Object.keys(this.players).length >= this.maxPlayers) this.preStart(newGame);
  }

  move(socketID, mark) {
    console.log("move 1");
    if (!this.canPlay) return;
    console.log("move 2");
    if(this.players[socketID].isLost === true) return;
    console.log("move 3");
    //if(this.players[socketID].isPlayed === true) return;
    //console.log("move 4");
    //this.players[socketID].isPlayed = true;

    this.board.move(this.players[socketID], mark);
  }

  endgame() {
    Object.keys(this.players).forEach(player => {
      console.log("check for bot... " + JSON.stringify(player));
      if (this.players[player].isBot && !this.players[player].isLost) {
        console.log("play...")
        this.move(player, Math.floor(Math.random() * 3));
      }
    });

    this.canPlay = false;
    this.win_check = false;
  }

  update() {
    if (!this.canPlay && !this.win_check) {
      this.winner = this.board.win();
      this.win_check = true;
    }

    Object.keys(this.sockets).forEach(playerID => {
      const socket = this.sockets[playerID];
      socket.emit("update", this.createUpdate());
    });
  }

  createUpdate() {
    let temporaly_cells = [];
    for (let i = 0; i < this.board.cells.length; i++)
      temporaly_cells.push({
        id: this.board.cells[i].id,
        mark: -2
      });
    return {
      canPlay: this.canPlay,
      cells: (this.canPlay || this.interval) ? temporaly_cells : this.board.cells,
      winner: this.winner,
      players: ObjToArr(this.players),
      isStart: this.isStart,
      canPlay: this.canPlay,
      roomID: this.name,
      size: this.board.size,
      game_time: this.game_time
    };
  }

  stopIntervals() {
    clearInterval(this.win_interval);
    clearInterval(this.game_interval);
    clearInterval(this.interval);
  }
}

function ObjToArr(arr) {
  return Object.keys(arr).map(function (key) {
    return arr[key];
  });
}
module.exports = Game;