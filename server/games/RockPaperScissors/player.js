class Player {
  constructor(id, username, img, isBot) {
    this.username = username;
    this.id = id;
    this.img = img;
    this.isLost = false;
    this.isPlayed = false;
    this.isBot = isBot;
  }
}

module.exports = Player;
