const rock = 0;
const paper = 1;
const scissors = 2;

class Board {
  constructor(players) {
    this.players = players;
    this.cells = [];
    this.counters = [0, 0, 0];

    Object.keys(this.players).forEach(player => {
      console.log("add cell");
      this.cells.push({
        id: this.players[player].id,
        mark: -1
      });
    });
    console.log("players " + JSON.stringify(this.players));
    console.log("board players " + Object.keys(this.players).length);
    console.log("cells " + this.cells.length);
  }

  move(player, mark) {
    console.log("move board " + player.id + " " + JSON.stringify(this.cells));
    let cell = this.cells.find(cell => cell.id === player.id);
    cell.mark = mark;
    this.counters[mark]++;
  }

  win() {
    let mark = -1;
    console.log("win cells " + JSON.stringify(this.cells));
    let isThereRock = this.cells.find(cell => cell.mark === rock);
    let isTherePaper = this.cells.find(cell => cell.mark === paper);
    let isThereScissors = this.cells.find(cell => cell.mark === scissors);
    console.log("win rock " + isThereRock + " paper " + isTherePaper + " scissors " + isThereScissors);
    if(isThereRock && isTherePaper && isThereScissors) //the game is a draw
      return {};

    //check for a winner
    if(this.counters[rock] === 1 && !isTherePaper) {
      return this.cells.find(cell => cell.mark === rock);
    }
    else if(this.counters[paper] === 1  && !isThereScissors) {
      return this.cells.find(cell => cell.mark === paper);
    }
    else if(this.counters[scissors] === 1  && !isThereRock ) {
      return this.cells.find(cell => cell.mark === scissors);
    }

    //eleminate lossers
    if(isThereRock && !isTherePaper) {
      this.cells.forEach(cell => {
        if (this.players[cell.id] && (cell.mark === scissors || cell.mark === -1))
          this.players[cell.id].isLost = true;
      });
      mark = rock;
    }
    else if(isTherePaper && !isThereScissors) {
      this.cells.forEach(cell => {
        if (this.players[cell.id] && (cell.mark === rock || cell.mark === -1))
          this.players[cell.id].isLost = true;
      });
      mark = paper;
    }
    else if(isThereScissors && !isThereRock) {
      this.cells.forEach(cell => {
        if (this.players[cell.id] && (cell.mark === paper || cell.mark === -1))
          this.players[cell.id].isLost = true;
      });
      mark = scissors;
    }
    return {mark: mark}; // there is no winner
  }
}

module.exports = Board;