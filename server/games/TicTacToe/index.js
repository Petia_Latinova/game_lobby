var Game = require("./game");

let games = {};
let playersCount = 0;
let connected = [];

  function connectUser(socket) {
    playersCount++;
    connected.push(socket);
    socket.on("joinGame", joinGame);
    socket.on("leaveGame", leaveGame);
    socket.on("playAgain", playAgain);
    socket.on("move", handleMove);
    socket.on("disconnect", leaveGame);
  };

  function playAgain() {
    games[this.roomName].reset();
  }

  function joinGame({
    username,
    img,
    maxPlayers,
    roomname
  }) {
    console.log("roomname " + roomname + " count " + playersCount + " maxPlayers " + maxPlayers + " this.roomname " + this.roomName)
    if (games[roomname]) {
      var game = games[roomname];
      console.log("canJoin " + game.canJoin + " maxPlayers " + game.maxPlayers + " players" + Object.keys(game.players).length)
      if (game.canJoin && game.maxPlayers === maxPlayers && Object.keys(game.players).length < maxPlayers) {
        this.roomName = roomname;
        game.addPlayer(this, username, img);
        console.log("join1: " + this.roomName);
      }
    }
    console.log("??? join1 " + this.roomName);
    if (!this.roomName || this.roomName === undefined) {
      var roomID = roomname;
      games[roomID] = new Game(roomID, maxPlayers);
      this.roomName = roomID;
      games[roomID].addPlayer(this, username, img);
      console.log("join2: " + this.roomName);
    }
    console.log("??? join2");
  }

  function leaveGame() {
    console.log("leave game");
    this.off("joinGame", joinGame);
    this.off("leaveGame", leaveGame);
    this.off("playAgain", playAgain);
    this.off("move", handleMove);
    this.off("disconnect", leaveGame);
    this.off("disconnect", removePlayer);
    if (this.roomName && games[this.roomName]) {
      console.log("leave game room exist");
      games[this.roomName].removePlayer(this);
      var players = games[this.roomName].sockets;
      console.log("leave game playerts left " + Object.keys(players).length);
      if (Object.keys(players).length === 0) {
        games[this.roomName].stopIntervals();
        delete games[this.roomName];
        console.log("delete: " + this.roomName);
      }
      console.log("leave: " + this.roomName);
      removePlayer(this.roomName);
      delete this.roomName;
    }
  }

  function handleMove(cellID) {
    games[this.roomName].move(this.id, cellID);
  }

  function onStart() {
    games[this.roomName].start();
  }

function removePlayer(roomName) {
    playersCount--;
    let index = connected.indexOf(this);
    connected.splice(index, 1);
  }

module.exports = connectUser;