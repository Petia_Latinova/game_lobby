/** Dotenv Environment Variables */
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

/** Connect to MongoDB */
const mongoose = require('mongoose');
require('./db/mongoose');

/** Built In Node Dependencies */
const path = require('path');
const fs = require('fs');

/** Logging Dependencies */
const morgan = require('morgan');
const winston = require('winston');
const { logger } = require('./config/logModule');

/** Passport Configuration */
const passport = require('passport');
require('./config/passport')(passport);

/** Express */
const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const cors = require('cors');
const helmet = require('helmet');
const enforce = require('express-sslify');

/** Socket IO */
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const {
    UPDATE_ROOM_USERS,
    GET_ROOMS,
    GET_ROOM_USERS,
    FILTER_ROOM_USERS,
    CREATE_MESSAGE_CONTENT
} = require('./actions/socketio');

const { Room } = require('./models/Room');
const { JOIN_ROOM } = require('./helpers/socketEvents');

/** Routes */
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const profileRoutes = require('./routes/profile');
const roomRoutes = require('./routes/room');

/** Middleware */
app.use(
    morgan('combined', {
        stream: fs.createWriteStream('logs/access.log', { flags: 'a' })
    })
);
app.use(morgan('dev'));

if (process.env.NODE_ENV === 'production') {
    /** Trust Proto Header for heroku */
    app.enable('trust proxy');
    app.use(helmet());
    app.use(enforce.HTTPS({ trustProtoHeader: true }));
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(expressValidator());
app.use(cors());
app.set('io', io);

/** Routes Definitions */
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/profile', profileRoutes);
app.use('/api/room', roomRoutes);

if (process.env.NODE_ENV !== 'production') {
    logger.add(
        new winston.transports.Console({
            format: winston.format.simple()
        })
    );
}

const TicTacToe = require('./games/TicTacToe/index.js');
const RockPaperScissors = require('./games/RockPaperScissors/index.js');
const games = {
    'Tic-Tac-Toe': {
        connectUser: TicTacToe
    },
    'Rock-Paper-Scissors': {
        connectUser: RockPaperScissors
    }
}

let userTypings = {};

/** Socket IO Connections */
io.on('connection', socket => {
    let currentRoomId = null;

    /** Socket Events */
    socket.on('disconnect', async () => {
        logger.info('User Disconnected');

        if (currentRoomId) {
            /** Filter through users and remove user from user list in that room */
            const roomState = await FILTER_ROOM_USERS({
                roomId: currentRoomId,
                socketId: socket.id
            });

            socket.broadcast.to(currentRoomId).emit(
                'updateUserList',
                JSON.stringify(
                    await GET_ROOM_USERS({
                        room: {
                            _id: mongoose.Types.ObjectId(currentRoomId)
                        }
                    })
                )
            );

            socket.broadcast.emit(
                'updateRooms',
                JSON.stringify({
                    room: await GET_ROOMS()
                })
            );
        }
    });

    /** Join User in Room */
    socket.on('userJoined', data => {
        currentRoomId = data.room._id;
        data.socketId = socket.id;
        JOIN_ROOM(socket, data);
        console.log("1 game: " + data.game);
        games[data.game].connectUser(socket);
    });

    /** User Exit Room */
    socket.on('exitRoom', data => {
        currentRoomId = null;
        socket.leave(data.room._id, async () => {
            socket.to(data.room._id).emit(
                'updateRoomData',
                JSON.stringify({
                    room: data.room
                })
            );

            /** Update room list count */
            socket.broadcast.emit(
                'updateRooms',
                JSON.stringify({
                    room: await GET_ROOMS()
                })
            );

            io.to(data.room._id).emit('receivedUserExit', data.room);
        });
    });

    /** User Typing Events */
    socket.on('userTyping', data => {
        if (!userTypings[data.room._id]) {
            userTypings[data.room._id] = [];
        } else {
            if (!userTypings[data.room._id].includes(data.user.nickname)) {
                userTypings[data.room._id].push(data.user.nickname);
            }
        }

        socket.broadcast
            .to(data.room._id)
            .emit('receivedUserTyping', JSON.stringify(userTypings[data.room._id]));
    });

    socket.on('removeUserTyping', data => {
        if (userTypings[data.room._id]) {
            if (userTypings[data.room._id].includes(data.user.nickname)) {
                userTypings[data.room._id] = userTypings[data.room._id].filter(
                    nickname => nickname !== data.user.nickname
                );
            }
        }

        socket.broadcast
            .to(data.room._id)
            .emit('receivedUserTyping', JSON.stringify(userTypings[data.room._id]));
    });

    /** Room Deleted Event */
    socket.on('roomDeleted', async data => {
        io.to(data.room._id).emit('roomDeleted', JSON.stringify(data));
        io.emit('roomListUpdated', JSON.stringify(data));
    });

    /** Room Added Event */
    socket.on('roomAdded', async data => {
        io.emit('roomAdded', JSON.stringify(data));
    });

    /** Room Updated Event */
    socket.on('roomUpdateEvent', async data => {
        io.in(data.room._id).emit('roomUpdated', JSON.stringify(data));
        io.emit('roomNameUpdated', JSON.stringify(data));
    });

    /** Reconnected: Update Reconnected User in Room */
    socket.on('reconnectUser', data => {
        currentRoomId = data.room._id;
        data.socketId = socket.id;
        if (socket.request.headers.referer.split('/').includes('room')) {
            socket.join(currentRoomId, async () => {
                socket.emit('reconnected');
                await UPDATE_ROOM_USERS(data);
            });
        }
    });
});

async function cleanUsers() {
    const rooms = await Room.find({})
        .populate('user', ['username'])
        .populate('users.lookup', ['username'])
        .select('-password')
        .exec();

    rooms.forEach(async room => {
        room.users = [];
        await room.save();
    });
}

(async () => {
    await cleanUsers();
})()

/** Serve static assets if production */
if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.resolve(__dirname, '../client', 'dist')));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../client', 'dist', 'index.html'));
    });
}

if (process.env.NODE_ENV !== 'test') {
    server.listen(process.env.PORT || 5000, () => {
        logger.info(`[LOG=SERVER] Server started on port ${process.env.PORT}`);
    });
}

module.exports = { app };
