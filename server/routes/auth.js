const _ = require('lodash');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const { User } = require('../models/User');
const gravatar = require('gravatar');
const socialAuthActions = require('../actions/socialAuthActions');
const crypto = require("crypto");
const nodemailer = require('nodemailer');

/** Middleware */
const {
    checkRegistrationFields,
    checkLoginFields,
    createErrorObject,
    customSocialAuthenticate,
    randomPassword
} = require('../middleware/authenticate');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'nbulost@gmail.com',
        pass: 'nbulostpassword1@'
    }
});

var confirmationMailTemplate = {
    from: 'nbulost@gmail.com',
    to: '',
    subject: 'Confirm Registration', 
    text: 'Please click the following link to complete your registration: EMAIL_TEXT'
};

var forbiddenMailTemplate = {
    from: 'nbulost@gmail.com',
    to: '',
    subject: 'Forgotten Password', 
    text: 'Your new password is:\r\n\r\nEMAIL_TEXT \r\n\r\nLOGIN_TEXT'
};

/**
 * @description  POST /register
 * @param  {} [checkRegistrationFields]
 * @param  {} request
 * @param  {} response
 * @access public
 */
router.post('/register', [checkRegistrationFields], (req, res) => {
    let errors = [];

    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            errors.push({ param: 'email', msg: 'Email is already taken' });

            if (user.username === req.body.username) {
                errors.push({ param: 'username', msg: 'Username is already taken' });
            }

            res.send({
                errors: createErrorObject(errors)
            }).end();
        } else {
            /** Assign Gravatar */
            const avatar = gravatar.url(req.body.email, {
                s: '220',
                r: 'pg',
                d: 'identicon'
            });

            console.log(crypto.randomBytes(16).toString("hex"));
            var token = req.body.email + crypto.randomBytes(16).toString("hex"); //get unique token, the non existing in DB email plus 16 random bytes

            const newUser = new User({
                nickname: req.body.nickname,
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
                image: avatar,
                confirmation_token: token
            });

            var port = req.headers.host.split(':')[1];
            //var confirmation_url = req.protocol + '://' + req.host + (port === 80 || port === 443 ? '' : ':' + port) + "/api/auth/confirmation/" + token;
            var confirmation_url = '';
            if (process.env.NODE_ENV === 'production') {
                confirmation_url = req.protocol + '://' + req.host + "/confirmation/" + token;
            }
            else {
                confirmation_url = req.protocol + '://' + req.host + ":" + process.env.CLIENT_PORT + "/confirmation/" + token;
            }
            var mailtext = JSON.parse(JSON.stringify(confirmationMailTemplate));//clone object serialize deserialize 
            mailtext.to = req.body.email;
            mailtext.text = mailtext.text.replace('EMAIL_TEXT', confirmation_url);
            transporter.sendMail(mailtext, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

            newUser
                .save()
                .then(userData => {
                    /*const user = _.omit(userData.toObject(), ['password']);

                    const token = jwt.sign(user, process.env.JWT_SECRET, {
                        expiresIn: 18000
                    });

                    res.status(200).send({
                        auth: true,
                        token: `Bearer ${token}`,
                        user
                    });*/

                    res.status(200).send({ success: true });
                })
                .catch(err => {
                    res.send({
                        err,
                        error: 'Something went wrong, Please check the fields again'
                    });
                });
        }
    });
});

/**
 * @description POST /forgottenpassword
 * @param  {} request
 * @param  {} response
 * @access public
 */
router.post('/forgottenpassword', async (req, res) => {
    let email = req.body.email;
    const user = await User.findOne({ email: req.body.email }).select('-password');

    if (!user) { /*for security reasons, always return ok from the server*/
        res.status(200).send({ success: true });
        return;
    }
    
    let password = randomPassword();

    let login_url = '';
    if (process.env.NODE_ENV === 'production') {
        login_url = req.protocol + '://' + req.host + "/login";
    }
    else {
        login_url = req.protocol + '://' + req.host + ":" + process.env.CLIENT_PORT + "/login";
    }
    var mailtext = JSON.parse(JSON.stringify(forbiddenMailTemplate));//clone object serialize deserialize 
    mailtext.to = req.body.email;
    mailtext.text = mailtext.text.replace('EMAIL_TEXT', password);
    mailtext.text = mailtext.text.replace('LOGIN_TEXT', login_url);
    transporter.sendMail(mailtext, async function (error, info) {
        if (error) {
            console.log(error);
            
            return res.status(500).send({
                error: "Can't send e-mail"
            });
        } else {
            user.password = password;
            await user.save();

            res.status(200).send({ success: true });
        }
    });
});

/**
 * @description POST /login
 * @param  {} checkLoginFields
 * @param  {} request
 * @param  {} response
 * @access public
 */
router.post('/login', checkLoginFields, async (req, res) => {
    const errors = [];
    const user = await User.findOne({
        $and: [{ email: req.body.email }, { confirmed: true }]
    }).select('-password');
    if (user === null) {
        errors.push({ param: 'nouserfound', msg: 'No User Found' });

        res.send({
            errors: createErrorObject(errors)
        }).end();
        return;
    }

    const token = jwt.sign(user.toObject(), process.env.JWT_SECRET, { expiresIn: 18000 });

    res.status(200).send({ auth: true, token: `Bearer ${token}`, user });
});

/**
 * @description POST /logout
 * @param  {} request
 * @param  {} response
 * @access public
 */
router.post('/logout', async (req, res) => {
    const user = await User.findOne({ username: req.body.username }).select('-password');

    if (!user) {
        return res.status(404).send({
            error: 'No User Found'
        });
    }

    res.status(200).send({ success: true });
});

router.post('/confirmation/:confirmation_token', async (req, res) => {
    console.log("confirmation")
    const user = await User.findOne({ confirmation_token: req.params.confirmation_token }).select('-password');
    console.log("confirmation: " + user)
    if (!user) {
        return res.status(404).send({
            error: 'No User Found'
        });
    }

    user.confirmed = true;
    user
        .save()
        .then(userData => {
            res.status(200).send({ success: true });
        })
        .catch(err => {
            res.send({
                err,
                error: 'Something went wrong, Please check the fields again'
            });
        });
});

/** Social Auth Routes */
router.get('/google', customSocialAuthenticate('google'));
router.get('/facebook', customSocialAuthenticate('facebook'));

/** Social Auth Callbacks */
router.get('/google/redirect', passport.authenticate('google'), socialAuthActions.google);
router.get('/facebook/redirect', passport.authenticate('facebook'), socialAuthActions.facebook);

module.exports = router;
