import _ from 'lodash';
import Vue from 'vue';
import Router from 'vue-router';
import vSelect from "vue-select";
import Home from './views/Home.vue';
import About from './views/About.vue';
import Login from '@/components/auth/Login.vue';
import Register from '@/components/auth/Register.vue';
import ForgottenPassword from '@/components/auth/ForgottenPassword.vue';
import Profile from '@/components/profile/Profile.vue';
import UserProfile from '@/components/user/UserProfile.vue';
import EditUserProfile from '@/components/user/EditUserProfile.vue';
import RoomList from '@/components/room/RoomList.vue';
import Room from '@/components/room/Room.vue';
import NotFound from '@/components/error/NotFound.vue';
import Confirmation from '@/components/auth/Confirmation.vue';
import SentConfirmation from '@/components/auth/SentConfirmation.vue';
import { checkUserData } from './helpers/user';
import store from './store';

Vue.use(Router);
Vue.component('v-select', vSelect);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/about',
            name: 'About',
            component: About,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            props: true,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
            props: true,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/forgottenpassword',
            name: 'ForgottenPassword',
            component: ForgottenPassword,
            props: true,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/user/:nickname',
            name: 'UserProfile',
            component: UserProfile,
            props: true,
            meta: {
                requiresAuth: true,
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/user/:nickname/edit',
            name: 'EditUserProfile',
            component: EditUserProfile,
            props: true,
            meta: {
                requiresAuth: true,
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/rooms',
            name: 'RoomList',
            component: RoomList,
            props: true,
            meta: {
                requiresAuth: true,
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/room/:id',
            name: 'Room',
            component: Room,
            meta: {
                requiresAuth: true,
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/profile/:nickname',
            name: 'Profile',
            component: Profile,
            meta: {
                requiresAuth: true,
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/confirmation/:confirmation_token',
            name: 'Confirmation',
            component: Confirmation,
            props: true,
            meta: {
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '/sentconfirmation',
            name: 'SentConfirmation',
            component: SentConfirmation,
            props: true,
            meta: {
                transitionName: 'router-anim',
                enterActive: 'animated fadeIn'
            }
        },
        {
            path: '*',
            component: NotFound
        }
    ]
});

router.beforeEach(async (to, from, next) => {
    await checkUserData(next);
    if (to.meta.requiresAuth) {
        if (localStorage.getItem('authToken') === null) {
            localStorage.clear();
            next({
                name: 'Login',
                params: { message: 'You are unauthorized, Please login to access' }
            });
        } else {
            next();
        }
    } else if (!_.isEmpty(to.meta) && !to.meta.requiresAuth) {
        if (localStorage.getItem('authToken')) {
            next({
                name: 'RoomList',
                params: { nickname: store.getters.getUserData.nickname }
            });
        } else {
            next();
        }
    } else {
        next();
    }
    next();
});

export default router;
